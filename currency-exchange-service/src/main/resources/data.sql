insert into exchange_value(id,from_currency,to_currency,conversion_multiple, port)
values (1001, 'USD', 'TL', 5.90, 0);
insert into exchange_value(id,from_currency,to_currency,conversion_multiple, port)
values (1002, 'TL', 'USD', 0.1694, 0);
insert into exchange_value(id,from_currency,to_currency,conversion_multiple, port)
values (1003, 'EUR', 'TL', 6.63, 0);
insert into exchange_value(id,from_currency,to_currency,conversion_multiple, port)
values (1004, 'TL', 'EUR', 0.1508, 0);
